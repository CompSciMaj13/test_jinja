#!/usr/bin/env python3
"""jinja2 test script"""

from jinja2 import Environment, PackageLoader

def main():
    """Return test template"""
    env = Environment(loader=PackageLoader('test_jinja'))

    template = env.get_template('test.j2')

    users = [
        {"url": "https://test", "username": "bob"},
        {"url": "https://test", "username": "bill"},
    ]

    return template.render(users=users)

if __name__ == '__main__':
    PAGE = main()
    print(PAGE)
